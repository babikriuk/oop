<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        include ('animal.php');
        include ('frog.php');
        include ('ape.php');

        $sheep = new Animal("shaun");
        echo "Name = ".$sheep->name."<br>"; // "shaun"
        echo "Legs = ".$sheep->legs."<br>"; // 4
        echo "Cold Blooded = ".$sheep->cold_blooded."<br><br>"; // "no"

        $sungokong = new Ape("kera sakti");
        echo "Name = ".$sungokong->name."<br>"; // "shaun"
        echo "Legs = ".$sungokong->legs."<br>"; // 4
        echo "Cold Blooded = ".$sungokong->cold_blooded."<br>"; // "no"
        $sungokong->yell();
        echo "<br><br>";

        $kodok = new Frog("buduk");
        echo "Name = ".$kodok->name."<br>"; // "shaun"
        echo "Legs = ".$kodok->legs."<br>"; // 4
        echo "Cold Blooded = ".$kodok->cold_blooded."<br>"; // "no"
        $kodok->jump();
        
    ?>
</body>
</html>